import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { TrailComponent } from './components/trail/trail.component';
import { StreetComponent } from './components/street/street.component';
import { CustomComponent } from './components/custom/custom.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { SportComponent } from './components/sport/sport.component';
import { TouringComponent } from './components/touring/touring.component';
import { LoginComponent } from './components/login/login.component';
import { CadastroUsuariosComponent } from './components/cadastro-usuarios/cadastro-usuarios.component';
import { CadastroClienteComponent } from './components/cadastro-cliente/cadastro-cliente.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';
import { AcompanhamentoPedidosComponent } from './components/acompanhamento-pedidos/acompanhamento-pedidos.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TrailComponent,
    StreetComponent,
    CustomComponent,
    ScooterComponent,
    SportComponent,
    TouringComponent,
    LoginComponent,
    CadastroUsuariosComponent,
    CadastroClienteComponent,
    CadastroMotosComponent,
    AcompanhamentoPedidosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
